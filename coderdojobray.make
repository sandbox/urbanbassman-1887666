; Core version
; ------------
 
core = 7.x
  
; API version
; ------------
  
api = 2
  
; Modules
; --------
projects[backup_migrate][type] = module
projects[backup_migrate][version] = 2.4
projects[calendar][type] = module
projects[calendar][version] = 3.4
projects[cit_importers][download][type] = git
projects[cit_importers][download][url] = urbanbassman@git.drupal.org:sandbox/urbanbassman/1516230.git
projects[cit_importers][type] = module
projects[context][type] = module
projects[context][version] = 3.0-beta6
projects[custom_search][type] = module
projects[custom_search][version] = 1.12
projects[ctools][type] = module
projects[ctools][version] = 1.2+7-dev
projects[date][type] = module
projects[date][version] = 2.6
projects[email][type] = module
projects[email][version] = 1.2
projects[entity][type] = module
projects[entity][version] = 1.0-rc3
projects[entityreference][type] = module
projects[entityreference][version] = 1.0
projects[features][type] = module
projects[features][version] = 1.0
projects[feeds][type] = module
projects[feeds][version] = 2.0-alpha8
projects[feeds_tamper][type] = module
projects[feeds_tamper][version] = 1.0-beta4
projects[feeds_xpathparser][type] = module
projects[feeds_xpathparser][version] = 1.0-beta4
projects[geolocation][type] = module
projects[geolocation][version] = 1.1
projects[job_scheduler][type] = module
projects[job_scheduler][version] = 2.0-alpha3
projects[libraries][type] = module
projects[libraries][version] = 2.0
projects[link][type] = module
projects[link][version] = 1.1
projects[menu_block][type] = module
projects[menu_block][version] = 2.3
projects[migrate][type] = module
projects[migrate][version] = 2.5
projects[migrate_extras][type] = module
projects[migrate_extras][version] = 2.5
projects[module_filter][type] = module
projects[module_filter][version] = 1.7
projects[pathauto][type] = module
projects[pathauto][version] = 1.2
projects[rules][type] = module
projects[rules][version] = 2.2
projects[scheduler][type] = module
projects[scheduler][version] = 1.0
projects[strongarm][type] = module
projects[strongarm][version] = 2.0
projects[token][type] = module
projects[token][version] = 1.5
projects[twitterfield][type] = module
projects[twitterfield][version] = 1.0-rc1
projects[views_bulk_operations][type] = module
projects[views_bulk_operations][version] = 3.1
projects[views][type] = module
projects[views][version] = 3.7
projects[webform][type] = module
projects[webform][version] = 3.18
projects[wordpress_migrate][type] = module
projects[wordpress_migrate][version] = 2.2
projects[wysiwyg][type] = module
projects[wysiwyg][version] = 2.2

; CoderDojo Features Packages
; ---------------------------
projects[coderdojo_wysiwyg_profile][type] = "module"
projects[coderdojo_wysiwyg_profile][download][type] = "git"
projects[coderdojo_wysiwyg_profile][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1875008.git"
projects[coderdojo_wysiwyg_profile][subdir] = "coderdojo"

; Themes
; --------

projects[omega][type] = "module"
projects[omega][subdir] = "custom"
projects[omega][version] = 3.1

;omega supporting modules
projects[omega_tools][type] = "module"
projects[omega_tools][subdir] = "custom"
projects[delta][type] = "module"
projects[delta][subdir] = "custom"

;projects[coderdojo_editor_role][type] = "module"
;projects[coderdojo_editor_role][download][type] = "git"
;projects[coderdojo_editor_role][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1887830.git"
;projects[coderdojo_editor_role][subdir] = "coderdojo"

projects[coderdojo_theme][type] = "theme"
projects[coderdojo_theme][download][type] = "git"
projects[coderdojo_theme][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1880900.git"
projects[coderdojo_theme][subdir] = "custom"
  
; Libraries
; ---------
libraries[jquery][download][type] = "file"
libraries[jquery][download][url] = "https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"
libraries[jqueryui][download][type] = "file"
libraries[jqueryui][download][url] = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.2/ckeditor_3.6.2.tar.gz"
libraries[ckeditor][directory_name] = "ckeditor"

